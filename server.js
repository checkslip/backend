const functions = require('firebase-functions');
const express = require('express')
const cors = require('cors');
const bodyParser = require('body-parser');

const app = express()
// Automatically allow cross-origin requests
app.use(cors({ origin: true }));

// parse body params and attache them to req.body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


var routes = require("./app/routes/route");
routes(app);

console.log(`Server running at`);
// Expose Express API as a single Cloud Function:
exports.api = functions.https.onRequest(app);